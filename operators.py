print("---------------------------")
print("This shows you that there are different ways to make the same IF comparisons")
print("if(a) and if(a == True) are the exact same")
print("if(not a) and if(a != True) are the exact same")

print("---------------------------")
print("If conditions - shortcut")
print("---------------------------")

a = True

if (a):
    print("The 'a' variable is true.")
elif (not a):
    print("The 'a' variable is false.")

b = False

if (b):
    print("The 'b' variable is true.")
elif (not b):
    print("The 'b' variable is false.")

print("---------------------------")
print("If conditions - long way")
print("---------------------------")

# Don't need to set the variables again. a is still True, b is still false

if (a == True):
    print("The 'a' variable is still true. Long")
elif (a != True):
    print("The 'a' variable is false now. Long")

if (b == True):
    print("The 'b' variable is true now. Long")
elif (b != True):
    print("The 'b' variable is still false. Long")

print("---------------------------")